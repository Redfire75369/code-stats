defmodule CodeStatsWeb.ProfileLive.DataProviders.TotalXP do
  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStatsWeb.ProfileLive.DataProviders

  @behaviour DataProviders.Behaviour

  @impl true
  @spec required_data() :: MapSet.t(DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([:recent_pulses, :cache])

  @impl true
  @spec retrieve(DataProviders.SharedData.t(), User.t()) :: DataProviders.CommonTypes.XPStat.t()
  def retrieve(shared_data, _user) do
    recent_xp =
      Enum.reduce(shared_data.recent_pulses, 0, fn %Pulse{} = p, acc ->
        acc + pulse_to_xps(p)
      end)

    total_xp = shared_data.cache.machines |> Map.values() |> Enum.sum()

    %DataProviders.CommonTypes.XPStat{
      total_xp: total_xp,
      recent_xp: recent_xp
    }
  end

  @impl true
  @spec update(DataProviders.CommonTypes.XPStat.t(), User.t(), Pulse.t(), User.Cache.t()) ::
          Data.t()
  def update(data, _user, pulse, _cache) do
    xps = pulse_to_xps(pulse)

    %DataProviders.CommonTypes.XPStat{
      total_xp: data.total_xp + xps,
      recent_xp: data.recent_xp + xps
    }
  end

  @spec pulse_to_xps(Pulse.t()) :: integer()
  defp pulse_to_xps(pulse) do
    Enum.reduce(pulse.xps, 0, fn %CodeStats.XP{} = x, acc ->
      acc + x.amount
    end)
  end
end
