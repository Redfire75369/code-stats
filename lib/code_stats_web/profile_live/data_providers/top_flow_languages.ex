defmodule CodeStatsWeb.ProfileLive.DataProviders.TopFlowLanguages do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.Profile.Queries.Flow
  alias CodeStats.User
  alias CodeStats.User.Pulse

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      data: [{String.t(), Flow.language_data()}]
    })
  end

  @impl true
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) ::
          Data.t()
  def retrieve(shared_data, _user) do
    update_data(shared_data.cache)
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(_data, _user, _pulse, cache) do
    update_data(cache)
  end

  @spec update_data(User.Cache.t()) :: Data.t()
  defp update_data(cache) do
    {:ok, languages} = Flow.top_languages(cache)

    %Data{
      data: languages |> Map.to_list() |> Enum.sort_by(&(-elem(&1, 1).amount))
    }
  end
end
