defmodule CodeStatsWeb.ProfileLive.DataProviders.DayInfo do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User.Flow
  alias CodeStats.User.Flow.Utils

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  @type most_focused() :: %{date: Date.t(), duration: non_neg_integer()}

  defmodule Data do
    deftypedstruct(%{
      average: number() | nil,
      top_day: %{date: Date.t(), xp: integer()} | nil,
      most_focused: CodeStatsWeb.ProfileLive.DataProviders.DayInfo.most_focused() | nil
    })
  end

  @impl true
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), CodeStats.User.t()) ::
          Data.t()
  def retrieve(shared_data, _user) do
    most_focused = get_most_focused(shared_data.cache.flows)
    top_day = get_top_day(shared_data.cache.dates)
    average = get_average(shared_data.cache.dates)

    %Data{
      average: average,
      top_day: top_day,
      most_focused: most_focused
    }
  end

  @impl true
  @spec update(Data.t(), CodeStats.User.t(), CodeStats.User.Pulse.t(), CodeStats.User.Cache.t()) ::
          Data.t()
  def update(data, _user, _pulse, cache) do
    # Only most recent flow can be updated live
    most_recent_flow = List.last(cache.flows)

    most_focused =
      if is_nil(most_recent_flow) do
        data.most_focused
      else
        {start_day, next_day} = Utils.divide_flow_minutes(most_recent_flow)

        start_day = flow_to_focused(start_day)
        next_day = flow_to_focused(next_day)

        old_most_focused =
          data.most_focused ||
            %{
              date: ~D[1970-01-01],
              duration: 0
            }

        Enum.max_by([old_most_focused, start_day, next_day], & &1.duration)
      end

    average = get_average(cache.dates)
    top_day = get_top_day(cache.dates)

    %Data{
      most_focused: most_focused,
      average: average,
      top_day: top_day
    }
  end

  @spec get_most_focused([CodeStats.User.Flow.t()]) ::
          %{date: Date.t(), flow_time: non_neg_integer()} | nil
  defp get_most_focused(flows) do
    flows
    |> Enum.reduce(%{}, fn flow, acc ->
      {start_day, next_day} = Utils.divide_flow_minutes(flow)

      acc
      |> Map.update(
        NaiveDateTime.to_date(start_day.start_time_local),
        start_day.duration,
        &(&1 + start_day.duration)
      )
      |> Map.update(
        NaiveDateTime.to_date(next_day.start_time_local),
        next_day.duration,
        &(&1 + next_day.duration)
      )
    end)
    |> Enum.max_by(&elem(&1, 1), fn -> nil end)
    |> then(fn
      {date, mins} -> %{date: date, duration: mins}
      nil -> nil
    end)
  end

  @spec get_top_day(CodeStats.User.Cache.dates_t()) :: %{date: Date.t(), xp: integer()} | nil
  defp get_top_day(days) do
    days
    |> Enum.max_by(&elem(&1, 1), fn -> nil end)
    |> then(fn
      {date, xp} -> %{date: date, xp: xp}
      nil -> nil
    end)
  end

  @spec get_average(CodeStats.User.Cache.dates_t()) :: number() | nil
  defp get_average(days) do
    case map_size(days) do
      0 -> nil
      size -> days |> Map.values() |> Enum.sum() |> then(&(&1 / size))
    end
  end

  @spec flow_to_focused(Flow.t()) :: most_focused()
  defp flow_to_focused(flow) do
    %{
      date: NaiveDateTime.to_date(flow.start_time_local),
      duration: flow.duration
    }
  end
end
