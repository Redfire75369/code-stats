defmodule CodeStatsWeb.ProfileLive.DataProviders.TopXPHours do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStatsWeb.ProfileLive.DataProviders

  @behaviour DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      data: Keyword.t({String.t(), integer()})
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, _user) do
    hours = shared_data.cache.hours |> Map.keys() |> Enum.sort(:asc)

    data =
      for hour <- hours do
        padded_hour = hour |> to_string() |> String.pad_leading(2, "0")
        {padded_hour, Map.fetch!(shared_data.cache.hours, hour)}
      end

    %Data{
      data: data
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(data, _user, pulse, _cache) do
    xps = pulse_to_xps(pulse)
    hour = to_string(pulse.sent_at_local.hour)

    new_hours =
      for {old_hour, old_xps} = old <- data.data do
        if old_hour == hour do
          {old_hour, old_xps + xps}
        else
          old
        end
      end

    %Data{
      data: new_hours
    }
  end

  @spec pulse_to_xps(Pulse.t()) :: integer()
  defp pulse_to_xps(pulse) do
    Enum.reduce(pulse.xps, 0, fn %CodeStats.XP{} = x, acc ->
      acc + x.amount
    end)
  end
end
