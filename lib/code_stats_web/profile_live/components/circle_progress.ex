defmodule CodeStatsWeb.ProfileLive.Components.CircleProgress do
  use Phoenix.Component

  def progress(assigns) do
    ~H"""
    <svg class="circle-progress" height={@radius * 2} width={@radius * 2} aria-hidden="true">
      <circle
        class="circle-progress-bar"
        stroke={@colour}
        stroke-width={@stroke}
        stroke-dasharray={circumference(@radius, @stroke)}
        style={"stroke-dashoffset:#{dash_offset(@radius, @stroke, @percent)}"}
        fill="transparent"
        r={normalized_radius(@radius, @stroke)}
        cx={@radius}
        cy={@radius}
      />
    </svg>

    <div class="circle-progress-content">
      <%= render_slot(@inner_block) %>
    </div>
    """
  end

  defp normalized_radius(radius, stroke), do: radius - stroke * 2

  defp circumference(radius, stroke) do
    normalized_radius(radius, stroke) * 2 * :math.pi()
  end

  defp dash_offset(radius, stroke, percent) do
    c = circumference(radius, stroke)
    c - percent / 100 * c
  end
end
