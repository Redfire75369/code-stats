defmodule CodeStatsWeb.ProfileLive.Components.Time do
  use CodeStatsWeb, :live_component

  @moduledoc """
  A <time> tag with the given class and short date.

  Requires:
    @date

  Optionally:
    @id
    @class
  """

  @impl true
  def render(assigns) do
    ~H"""
    <time class={@class} datetime={Date.to_iso8601(@date)}>
      <%= Calendar.strftime(@date, "%b %-d, %Y") %>
    </time>
    """
  end
end
