defmodule CodeStatsWeb.Components.LicenseKey do
  use Phoenix.Component
  alias CodeStatsWeb.Components.Error

  def error_tag(assigns) do
    ~H"""
    <%= case @error do %>
      <% nil -> %>
      <% {"not_found", []} -> %>
        <Error.error_tag>is not a valid license key</Error.error_tag>
      <% {"subscription_invalid", []} -> %>
        <Error.error_tag>has a subscription that has been ended</Error.error_tag>
      <% {"invalid", []} -> %>
        <Error.error_tag>
          has been refunded or disputed
        </Error.error_tag>
      <% {"error", []} -> %>
        <Error.error_tag>
          There was an unknown error trying to verify the license key.
        </Error.error_tag>
      <% {_, _} = error -> %>
        <Error.error_tag><%= Error.translate_error(error) %></Error.error_tag>
    <% end %>
    """
  end
end
