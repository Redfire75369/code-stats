defmodule CodeStatsWeb.Components.Error do
  use Phoenix.Component

  @doc """
  Generates tag for inlined form input errors.
  """
  def form_error(assigns) do
    ~H"""
    <%= if @form.errors[@field] do %>
      <.error_tag><%= translate_error(@form.errors[@field]) %></.error_tag>
    <% end %>
    """
  end

  def error_tag(assigns) do
    ~H"""
    <p class="help-block has-error"><%= render_slot(@inner_block) %></p>
    """
  end

  @doc """
  Translates an error message using gettext.
  """
  def translate_error({msg, opts}) do
    # Because error messages were defined within Ecto, we must
    # call the Gettext module passing our Gettext backend. We
    # also use the "errors" domain as translations are placed
    # in the errors.po file.
    # Ecto will pass the :count keyword if the error message is
    # meant to be pluralized.
    # On your own code and templates, depending on whether you
    # need the message to be pluralized or not, this could be
    # written simply as:
    #
    #     dngettext "errors", "1 file", "%{count} files", count
    #     dgettext "errors", "is invalid"
    #
    if count = opts[:count] do
      Gettext.dngettext(CodeStatsWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(CodeStatsWeb.Gettext, "errors", msg, opts)
    end
  end

  def translate_error(msg) do
    Gettext.dgettext(CodeStatsWeb.Gettext, "errors", msg)
  end
end
