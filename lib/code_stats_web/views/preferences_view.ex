defmodule CodeStatsWeb.PreferencesView do
  use CodeStatsWeb, :view
  alias CodeStatsWeb.Components.LicenseKey
  alias CodeStats.User.Paid.Level

  def get_flash(conn, type), do: CodeStatsWeb.LayoutView.get_flash(conn, type)
end
