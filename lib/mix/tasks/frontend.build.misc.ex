defmodule Mix.Tasks.Frontend.Build.Misc do
  use MBU.BuildTask, auto_path: false
  import CodeStats.FrontendConfs

  @shortdoc "Copy misc assets to target dir"

  @deps []

  def in_path(), do: base_src_path()
  def out_path(), do: base_dist_path()

  task _ do
    File.cp!(Path.join(in_path(), "robots.txt"), Path.join(out_path(), "robots.txt"))
  end
end
