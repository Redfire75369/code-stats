defmodule CodeStats.Utils.GeoTypes do
  @typedoc """
  Map of latitude/longitude coordinates.
  """
  @type coord_t :: %{lat: float(), lon: float()}
end
