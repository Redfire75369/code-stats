defmodule CodeStats.User.Paid.LevelUtils do
  alias CodeStats.User.Paid.Level
  alias CodeStats.PaidUser

  @doc """
  Get paid level of specified user who can be nil.
  """
  @spec level_for_user(PaidUser.t() | nil) :: Level.t()
  def level_for_user(user)

  def level_for_user(nil), do: Level.none()
  def level_for_user(user), do: user.paid_level
end
