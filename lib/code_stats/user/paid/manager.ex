defmodule CodeStats.User.Paid.Manager do
  @doc """
  Manager that monitors license keys in the system and verifies that their subscriptions are
  still valid and that they correspond to the correct unlock level.
  """

  use GenServer

  require Logger

  import CodeStats.Utils.TypedStruct
  import Ecto.Query, only: [from: 2]

  alias CodeStats.Repo
  alias CodeStats.User.Paid.LicenseUtils
  alias CodeStats.PaidUser
  alias CodeStats.User.Paid.Level
  alias CodeStats.User.Paid.ProductUtils

  defmodule Options do
    deftypedstruct(%{
      name: GenServer.name(),
      check_interval: non_neg_integer()
    })
  end

  defmodule State do
    deftypedstruct(%{
      check_interval: non_neg_integer()
    })
  end

  @spec start_link(Options.t()) :: GenServer.on_start()
  def start_link(%Options{} = opts) do
    GenServer.start_link(__MODULE__, opts, name: opts.name)
  end

  @impl GenServer
  @spec init(Options.t()) :: {:ok, State.t()}
  def init(%Options{} = opts) do
    schedule_execution(opts.check_interval)
    {:ok, %State{check_interval: opts.check_interval}}
  end

  @impl GenServer
  @spec handle_info(term(), State.t()) :: {:noreply, State.t()}
  def handle_info(msg, state)

  def handle_info(:process, state) do
    process_user()
    schedule_execution(state.check_interval)
    {:noreply, state}
  end

  @spec schedule_execution(non_neg_integer()) :: :ok
  defp schedule_execution(check_interval) do
    Process.send_after(self(), :process, check_interval * 60 * 1_000)
    :ok
  end

  @spec process_user() :: :ok
  defp process_user() do
    user = find_user()

    if user != nil do
      case check_user(user) do
        {:ok, purchase_data} ->
          product = ProductUtils.get_product_for_variant(purchase_data.variants)

          if product != nil do
            update_user_level(user, product.unlock_level)
            increment_user_validity(user)
          else
            raise "Could not find product data for variant #{inspect(purchase_data.variants)}!"
          end

        error ->
          Logger.error("User #{user.id} license invalid: #{inspect(error)}")
          invalidate_user(user)
      end
    end

    :ok
  end

  @spec find_user() :: PaidUser.t() | nil
  defp find_user() do
    now = Date.utc_today()

    from(pu in PaidUser,
      where:
        (pu.subscription_valid_until < ^now or is_nil(pu.subscription_valid_until)) and
          not is_nil(pu.license_key),
      limit: 1
    )
    |> Repo.one()
  end

  @spec check_user(PaidUser.t()) :: {:ok, Gumroad.Api.PurchaseData.t()} | term()
  defp check_user(user) do
    case LicenseUtils.verify_license(user.license_key) do
      {:ok, data} -> {:ok, data}
      error -> error
    end
  end

  @spec update_user_level(PaidUser.t(), Level.t()) :: :ok
  defp update_user_level(user, level) do
    changeset = PaidUser.level_changeset(user, %{paid_level: level})

    Repo.update!(changeset)
  end

  @spec increment_user_validity(PaidUser.t()) :: :ok
  defp increment_user_validity(user) do
    now = Date.utc_today()
    then = now |> Date.add(LicenseUtils.license_check_interval())

    changeset =
      PaidUser.subscription_validity_changeset(user, %{
        subscription_valid_until: then
      })

    Repo.update!(changeset)
  end

  @spec invalidate_user(PaidUser.t()) :: :ok
  defp invalidate_user(user) do
    changeset = PaidUser.invalidate_changeset(user)

    Repo.update!(changeset)
  end
end
