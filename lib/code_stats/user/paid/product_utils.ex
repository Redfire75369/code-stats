defmodule CodeStats.User.Paid.ProductUtils do
  import Ecto.Query, only: [from: 2]

  alias CodeStats.User.Paid.Product
  alias CodeStats.Repo

  @doc """
  Get product that matches the given variant string.
  """
  @spec get_product_for_variant(String.t()) :: Product.t() | nil
  def get_product_for_variant(variant) do
    from(p in Product, where: p.variant_name == ^variant)
    |> Repo.one()
  end
end
