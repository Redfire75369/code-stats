defmodule CodeStats.User.Flow do
  import CodeStats.Utils.TypedStruct

  @typedoc """
  Flow represents a user's flow state. In this state the user has been programming without pause
  for a certain amount of time, i.e. they are "in the zone".
  """
  deftypedstruct(%{
    # When the flow started, in UTC
    start_time: DateTime.t(),
    # When the flow started, in user's local time
    start_time_local: NaiveDateTime.t(),
    # How long the flow has lasted, in minutes
    duration: {non_neg_integer(), 0},
    # How much XP has been accumulated during the flow
    xp: {integer(), 0},
    # Languages written during the flow
    languages: {%{integer() => integer()}, %{}}
  })

  @typedoc "Format of flow languages in cache (map from language ID to amount)"
  @type cache_lang_t :: %{optional(String.t()) => integer()}

  @typedoc "Format of flow data in cache."
  @type cache_t :: %{required(String.t()) => String.t() | integer() | cache_lang_t()}

  @doc """
  Maximum length of pause (time with no pulses) before flow is ended, in minutes.
  """
  @spec max_pause() :: pos_integer()
  def max_pause(), do: Application.get_env(:code_stats, :flow_max_pause)

  @doc """
  Minimum length of flow to be counted, in minutes.
  """
  @spec min_length() :: pos_integer()
  def min_length(), do: Application.get_env(:code_stats, :flow_min_length)

  @doc """
  Minimum amount of XP per minute for a flow to be counted.
  """
  @spec min_xp_ratio() :: pos_integer()
  def min_xp_ratio(), do: Application.get_env(:code_stats, :flow_min_xp_ratio)

  @doc """
  Get string key used in cache DB format for given Flow field.
  """
  @spec cache_key(:start_time | :start_time_local | :duration | :amount | :languages) ::
          String.t()
  def cache_key(type)

  def cache_key(:start_time), do: "s"
  def cache_key(:start_time_local), do: "sl"
  def cache_key(:duration), do: "d"
  def cache_key(:amount), do: "x"
  def cache_key(:languages), do: "l"
end
